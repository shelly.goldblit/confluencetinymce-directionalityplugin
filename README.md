# TinyMCE Directionality Plugin for Confluence

This plugin enable RTL editing in Confluence.

Supported versions: Confluence server 6.14 and further.

## This plugin adds directionality buttons to the editor toolbar

![image](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/uploads/871fc3abd320599cefd06e68f1e297ec/image.png)

Page title is center justified to fit both RTL and LTR pages.

Note, that when selecting a direction, the corresponding alignment button is also activated

![image](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/uploads/de28487f85f9a07cbbdc25c6ae1ab6d4/image.png) & 
![image](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/uploads/f2775c231a65fc6baf3cebcde9edfacd/image.png)

## This plugin supports RTL task lists and RTL tables

#### Task list in edit mode

![image](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/uploads/d3dde116cd3dc5469e03a50394d4fa35/image.png)

#### Task list in view mode

![image](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/uploads/e4d428cbb089999ecf1c4ef66e272c93/image.png)

#### Sub tasks are also supported

![image](/uploads/0497ade6e86b720258022ea9a3880070/image.png)

## Installation Instructions

For a stable production release, please download the latest version in the [Releases tab](https://gitlab.com/shelly.goldblit/confluencetinymce-directionalityplugin/-/releases) and follow these instructions for install and configuration.

1. Log into your Confluence instance as an admin.
2. Click the admin dropdown and choose **Manage apps**.
3. Click on **Upload App**.\
<sub>   The _Upload app_ screen loads</sub>.
4. Click on **Choose File** and select the plugin file.
5. Click on **Upload**.
6. You're all set! \
<sub>   Click **Close** in the _Installed and ready to go_ dialog.</sub>


## Build Instructions

Download the source, and install Atllasian SDK (See [Attlassian documentation](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK)).


* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help  -- prints description for all commands in the SDK

